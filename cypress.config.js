const { defineConfig } = require("cypress");

module.exports = defineConfig({
  projectId: "ncz64k",
  viewportWidth: 1800,
  viewportHeight: 800,
  chromeWebSecurity: false,
  e2e: {
    setupNodeEvents(on, config) {
      return config;
    },
  },
});
