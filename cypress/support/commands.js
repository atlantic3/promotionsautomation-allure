// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

Cypress.Commands.add('p1', (url) => {
    if (!url) throw new Error('You need to provide a url');
    cy
      .get(url)
    
    
    const log = Cypress.log({
    name: 'Data Websol',
    // shorter name for the Command Log
    displayName: 'Data Websol',
    message: `${key}, ${value}`,
    consoleProps: () => {
      // return an object which will
      // print to dev tools console on click
      return {
        Key: key,
        Value: value,
        'Session Storage': window.sessionStorage,
      }
    },
  })

  });
  
  Cypress.Commands.add('eliminarModal', () => {
    //elimina modal reto
  cy.get('.MuiDialog-container').should((_) => {}).then(($modal) => {
      if ($modal.length) {
          cy.get('.MuiDialog-container').invoke('remove')
          cy.get('.MuiDialog-root').invoke('remove')
      } else{
          cy.log("NO HAY MODAL")
      }
      })
  
  })

  Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
  })

  import 'cypress-wait-until';
  
  Cypress.Commands.add('iweb', (url) => {
    if (!url) throw new Error('You need to provide a url');
    cy
      .get(url)
    
    
    const log = Cypress.log({
    name: 'Data Websol',
    // shorter name for the Command Log
    displayName: 'Data Websol',
    message: `${key}, ${value}`,
    consoleProps: () => {
      // return an object which will
      // print to dev tools console on click
      return {
        Key: key,
        Value: value,
        'Session Storage': window.sessionStorage,
      }
    },
  })
  
  });
    
  let LOCAL_STORAGE_MEMORY = {};

  Cypress.Commands.add("saveLocalStorage", () => {
  Object.keys(localStorage).forEach(key => {
    LOCAL_STORAGE_MEMORY[key] = localStorage[key];
  });
});

  Cypress.Commands.add("restoreLocalStorage", () => {
  Object.keys(LOCAL_STORAGE_MEMORY).forEach(key => {
    localStorage.setItem(key, LOCAL_STORAGE_MEMORY[key]);
  });
});


  

  