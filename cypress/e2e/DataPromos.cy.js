
import { credentials, invitado} from '../support/e2e.js'

beforeEach(() => {
    cy.restoreLocalStorage();

  });
  
  afterEach(() => {
    cy.saveLocalStorage();
  });

  var valores = new Object()
  const valoresCrm = []
  const valoresWebsol = []


describe.only ( 'Validar extracción de data WEB', ()=> {

    it( 'Validar Login', ()=>{

        //ingresar
        cy.visit('https://www.casinoatlanticcity.com/casino-online')
        cy.get('.headerLoginButtons_themed-login-buttons__BNdao > div > .clmc-header-button').click({ waitForAnimations: true })    
        cy.get('#user').type(credentials.usrweb[0])
        cy.get('#password').type(credentials.usrweb[0])
        cy.get('.clmc-dark-form > .clmc-btn-primary').click()
        cy.waitUntil(() => cy.get('[aria-label="Depositar"]').should('have.length', 1), { timeout: 10000 });
        cy.eliminarModal()

    })
    it("Validar data web Top", ()=> {
          //TOP
          
          cy.visit('https://www.casinoatlanticcity.com/casino-online/promociones/top-atlantic')
          cy.wait(3500)
          cy.eliminarModal()
          cy.wait(2500)

  
          //cambio ID
          cy.get('.styles_openButton__oFAO0').click({force: true})
          cy.get('.styles_boxChangeId__NAAMr > input').clear().type(invitado.idOrigen)
          cy.get('.styles_boxChangeId__NAAMr > button').click({force: true})
          cy.wait(3500)
          cy.contains('Avance').click()


          //Extraer data
          cy.wait(4500) 
          cy.eliminarModal()

        //   cy.get(['data-testid=textFreeCell"']).invoke('attr', 'data-test-value').then(($puesto)=> {

    
        //     const puestoTop = $puesto.text()

        //     valores.push(`puestoTop: ${puestoTop}`)
        //     cy.log(puestoTop)
        //     cy.log('ARREGLO:' +     valores.puestoTop)
        // })
        cy.contains("¡Empieza a jugar!").should((_) => {}).then(($a) => {
            if(!$a.length){

            
        cy.get('#idPuesto').should((_) => {}).then(($btn) => {
            if ($btn.length) {
                cy.get('#idPuesto').then(($puesto)=> {
                const puestoTop = $puesto.text() 
                cy.log(puestoTop)
                valores.puestoTop= puestoTop
                cy.log('ARREGLO:' +     valores[9])
            })
      
         } else {
            cy.get('[data-testid="imageFreeCell"]').then(($puesto)=> {
    
                const puestoTop = $puesto.attr("data-test-value")
    
                valores.puestoTop = puestoTop
                cy.log(puestoTop)
                cy.log(valores)
                cy.log('ARREGLO:' +   valores.puestoTop)
                })
            

         }
        
        })
        
       
          cy.get('#idPuntos').then(($puntos)=> {
              var puntajeTop = $puntos.text()

            while(puntajeTop.includes(",")){
                var puntajeTopA = puntajeTop.replace(",","")
                puntajeTop = puntajeTopA
            }

             
              cy.log(puntajeTopA)
  
              valores.puntajeTop = puntajeTopA
              cy.log(puntajeTop)
              cy.log('ARREGLO:' +     valores.puntajeTop)
          })
        
            
          cy.get('[data-testid="moneyAmountFreeCell"]').should((_) => {}).then(($premio)=> {
            if ($premio.length) {
            const premioTop = $premio.attr("data-test-value") 
            
              cy.log(premioTop)
              valores.premioTop = premioTop

              
              
              cy.log('ARREGLO:' +     valores[2])
              cy.log(valores)
            }
            else{
                valores.premioTop = "0"
            }
              // valores.forEach(fun ction(entry){
              //     cy.log(entry)
              //    })
              // cy.log(valores.length)
          })
        }else{
            valores.premioTop = "0"
            valores.puntajeTop = "0"
            valores.puestoTop = "0"
        }
    })
    })

    it("Validar data web Estelar", () =>{

           //ESTELAR
        cy.visit('https://www.casinoatlanticcity.com/casino-online/promociones/sorteo-estelar')
      
        
        //Extraer data
        cy.wait(3500)
        cy.eliminarModal()
        cy.contains('Canje').click()

        cy.contains('¡Sorteo en curso!').should((_) => {}).then(($btn) => {
            if (!$btn.length) {
                cy.contains('¡Pronto será el sorteo!').should((_) => {}).then(($btn2) => {                         
                if (!$btn2.length) {
                    cy.get('#idpuntaje').then(($puntos)=> {
                        const puntajeEstelar = $puntos.text()
                        //cy.writeFile('cypress/fixtures/users.json', {puntajeEstelar: puntajeEstelar})  
                        cy.log(puntajeEstelar)
                        valores.puntajeEstelar = puntajeEstelar
                        cy.log('ARREGLO:' +     valores[3])
                    })
                    cy.get('.bg-gradient-box-opciones-sorteo-estelar > .promos-v3-flex > .promos-v3-text-white').then(($cupon)=> {
                        const cupEstelar = $cupon.text()
                        //cy.writeFile('cypress/fixtures/users.json', {cupEstelar: cupEstelar})  
                        cy.log(cupEstelar)
                        valores.cuponEstelar = cupEstelar
                        cy.log('ARREGLO:' +     valores[4])
                    })
                    cy.contains("Equivalen a").then(($opcionesdisp)=> {
                        const opcdisEstelar = $opcionesdisp.attr("data-test-value")
                        //cy.writeFile('cypress/fixtures/users.json', {opcdisEstelar: opcdisEstelar})   
                        cy.log(opcdisEstelar)
                        valores.opcionesDisponiblesEstelar = opcdisEstelar
                        cy.log('ARREGLO:' +     valores[5])
                    })
                    }
                    else{
                        cy.get('.bg-gradient-box-opciones-sorteo-estelar > .promos-v3-flex > .promos-v3-text-white').then(($cupon)=> {
                            const cupEstelar = $cupon.text()
                            //cy.writeFile('cypress/fixtures/users.json', {cupEstelar: cupEstelar})  
                            cy.log(cupEstelar)
                            valores.cuponEstelar= cupEstelar
                            valores.opcionesDisponiblesEstelar = "0"
                            valores.puntajeEstelar = "0"
                            cy.log('ARREGLO:' +     valores[4])
                        })
                        
                    }
                })
            }
            else{
                cy.get('.bg-gradient-box-opciones-sorteo-estelar > .promos-v3-flex > .promos-v3-text-white').then(($cupon)=> {
                    const cupEstelar = $cupon.text()
                    //cy.writeFile('cypress/fixtures/users.json', {cupEstelar: cupEstelar})  
                    cy.log(cupEstelar)
                    valores.cuponEstelar= cupEstelar
                    cy.log('ARREGLO:' +     valores[4])
                    valores.opcionesDisponiblesEstelar = "0"
                    valores.puntajeEstelar = "0"
                })
        
            }
        
        })

           
    })

    it("Validar data web Mega", ()=> {

        //MEGA
    cy.visit('https://www.casinoatlanticcity.com/casino-online/promociones/mega-torneo')
    cy.wait(4000)
    cy.eliminarModal()
    cy.contains('Avance').click()
    cy.wait(400)
    

    // Validar torneo activo
    cy.get('.btn').should((_) => {}).then(($btn) => {
        if (!$btn.length) {

        cy.get('#idPuesto').should((_) => {}).then(($btn) => {
            if ($btn.length) {
                cy.get('#idPuesto').then(($puesto)=> {
                    const puestoMega = $puesto.text() 
                    cy.log(puestoMega)
                    valores.puestoMega= puestoMega
                    cy.log('ARREGLO:' +     valores[9])
                })
      
         } else {
            cy.get('[data-testid="imageFreeCell"]').then(($puesto)=> {
                const puestoMega = $puesto.attr("data-test-value") 
                cy.log(puestoMega)
                valores.puestoMega= puestoMega
                cy.log('ARREGLO:' +     valores[6])
            })
            

         }
        
        })
        
       

        //extraer data
        cy.get('#idPuntos').then(($puntos)=> {
            var puntosMega = $puntos.text()
            var puntajeMegaA = puntosMega.replaceAll(",","")
            puntosMega = puntajeMegaA
            
            
            cy.log(puntajeMegaA) 
            cy.log(puntosMega)
            valores.puntosMega= puntajeMegaA
            cy.log('ARREGLO:' +     valores[7])
        })
        cy.get('[data-testid="moneyAmountFreeCell"]').should((_) => {}).then(($premio)=> {

            if ($premio.length) {
            
            const premioMega = $premio.attr("data-test-value") 
            cy.log(premioMega)
            valores.premioMega= premioMega
            cy.log('ARREGLO:' +     valores[8])
            }else{
                valores.premioMega= "0"
            }
        })
        } else{
            cy.log("TORNEO NO ACTIVO")
            valores.torneoMega= "TORNEO INACTIVO"
        }
    
    })

    })

    
    it("Validar data web Mesas", ()=> {

        //MESAS
    cy.visit('https://www.casinoatlanticcity.com/casino-online/promociones/torneo-mesas')
    cy.wait(2500)
    cy.eliminarModal()
    cy.contains('Avance').click()
    cy.wait(4000)

    // Validar torneo activo
    cy.get('.btn').should((_) => {}).then(($btn) => {
        if (!$btn.length) {
          

        cy.get('#idPuesto').should((_) => {}).then(($btn) => {
            if ($btn.length) {
                cy.get('#idPuesto').then(($puesto)=> {
                    const puestoMesas = $puesto.text() 
                    cy.log(puestoMesas)
                    valores.puestoMesas= puestoMesas
                    cy.log('ARREGLO:' +     valores[9])
                })
      
         } else {
            cy.get('[data-testid="imageFreeCell"]').then(($puesto)=> {
    
                const puestoTop = $puesto.attr("data-test-value")
    
                valores.puestoTop = puestoTop
                cy.log(puestoTop)
                cy.log(valores)
                cy.log('ARREGLO:' +   valores.puestoTop)
                })
            

         }
        
        })
        
        cy.get('#idPuntos').then(($puntos)=> {
            var puntosMesas = $puntos.text() 
            cy.log(puntosMesas)

            while(puntosMesas.includes(",")){
                var puntosMesasA = puntosMesas.replace(",","")
                puntosMesas = puntosMesasA
            }
            cy.log(puntosMesas) 
            valores.puntosMesas = puntosMesasA
            cy.log('ARREGLO:' +     valores[10])

        })
        cy.get('[data-testid="moneyAmountFreeCell"]').should((_) => {}).then(($premio)=> {

            if ($premio.length) {
            
            const premioMesas = $premio.attr("data-test-value")     
            cy.log(premioMesas)
            valores.premioMesas= premioMesas
            cy.log('ARREGLO:' +     valores[11])
            }
            else
            {
                valores.premioMesas = "0"
            }

        })
        } else{
            cy.log("TORNEO NO ACTIVO")
            valores.torneoMesas = "TORNEO INACTIVO"

        }
    
    })
    
    })


    it("Validar data web Vip", ()=> {

            //VIP
    cy.visit('https://www.casinoatlanticcity.com/casino-online/promociones/sorteo-vip-royal')
    cy.wait(2500)
    cy.eliminarModal()
    cy.contains('Canje').click()
    cy.wait(4000)

        cy.contains('¡Sorteo en curso!').should((_) => {}).then(($btn) => {
        if (!$btn.length) {
            cy.contains('¡Pronto será el sorteo!').should((_) => {}).then(($btn2) => {                         
                if (!$btn2.length) {
            
                cy.get('#idpuntaje').then(($puntos)=> {
                    const puntosVip = $puntos.text() 
                    cy.log(puntosVip)
                    valores.puntosVip= puntosVip
                    cy.log('ARREGLO:' +     valores[12])
                })
                cy.get('.bg-gradient-box-opciones-vip-royal > .promos-v3-flex > .promos-v3-text-white').then(($cupon)=> {
                    const cuponVip = $cupon.text() 
                    cy.log(cuponVip)
                    valores.cuponVip= cuponVip
                    cy.log('ARREGLO:' +     valores[13])
                })
                cy.contains("Equivalen a").then(($opcionesdisp)=> {
                    const opcionesdispVip = $opcionesdisp.attr("data-test-value")
                    cy.log(opcionesdispVip)
                    valores.opcionesDisponiblesVip=opcionesdispVip
                    cy.log('ARREGLO:' +     valores[14])
                })}

                else{
                    cy.get('.bg-gradient-box-opciones-vip-royal > .promos-v3-flex > .promos-v3-text-white').then(($cupon)=> {
                        const cuponVip = $cupon.text() 
                        cy.log(cuponVip)
                        valores.cuponVip= cuponVip
                        cy.log('ARREGLO:' +     valores[13])
                        valores.puntosVip= "0"
                        valores.opcionesDisponiblesVip= "0"
                    })

                }
            })
        } else{
            cy.get('.bg-gradient-box-opciones-vip-royal > .promos-v3-flex > .promos-v3-text-white').then(($cupon)=> {
                const cuponVip = $cupon.text() 
                cy.log(cuponVip)
                valores.cuponVip= cuponVip
                cy.log('ARREGLO:' +     valores[13])
                valores.puntosVip= "0"
                valores.opcionesDisponiblesVip= "0"
            })

        }


        })

    })

    it("Validar data web Sueños", ()=> {
    
    //SUENOS
    cy.visit('https://www.casinoatlanticcity.com/casino-online/promociones/sorteo-suenos')
    cy.wait(2500)
    cy.eliminarModal()
    cy.contains('Canje').click()
    cy.wait(4000)

    cy.contains('¡Sorteo en curso!').should((_) => {}).then(($btn) => {
        if (!$btn.length) {
            cy.contains('¡Pronto será el sorteo!').should((_) => {}).then(($btn2) => {                         
            if (!$btn2.length) {
                cy.get('#idpuntaje').then(($puntos)=> {
                    const puntosSueños = $puntos.text() 
                    cy.log(puntosSueños)
                    valores.puntosSueños= puntosSueños
                    cy.log('ARREGLO:' +     valores[15])
                })
                cy.get('.bg-gradient-box-opciones-suenos > .promos-v3-flex > .promos-v3-text-white').then(($cupon)=> {
                    const cuponSueños = $cupon.text() 
                    cy.log(cuponSueños)
                    valores.cuponSueños= cuponSueños
                    cy.log('ARREGLO:' +     valores[16])
                })
                cy.contains("Equivalen a").then(($opcionesdisp)=> {
                    const opcionesdispSueños = $opcionesdisp.attr("data-test-value")
                    cy.log(opcionesdispSueños)
                    valores.opcionesDisponiblesSueños= opcionesdispSueños
                    cy.log('ARREGLO:' +     valores[14])
                })
            }
            else{
            cy.get('.bg-gradient-box-opciones-suenos > .promos-v3-flex > .promos-v3-text-white').then(($cupon)=> {
                const cuponSueños = $cupon.text() 
                cy.log(cuponSueños)
                valores.cuponSueños= cuponSueños
                cy.log('ARREGLO:' +     valores[16])
                valores.opcionesDisponiblesSueños= "0"
                valores.puntosSueños= puntosSueños= "0"

            })
         }})

        }  else{
            cy.get('.bg-gradient-box-opciones-suenos > .promos-v3-flex > .promos-v3-text-white').then(($cupon)=> {
                const cuponSueños = $cupon.text() 
                cy.log(cuponSueños)
                valores.cuponSueños= cuponSueños
                cy.log('ARREGLO:' +     valores[16])
                valores.opcionesDisponiblesSueños= "0"
                valores.puntosSueños= puntosSueños= "0"
            })
         }
    })


    
    
    })
  

      it("Guardar Data", ()=> {
    

            cy.writeFile('cypress/fixtures/dataWeb.json', valores)
            cy.readFile('cypress/fixtures/dataWeb.json')
    
        
      })
      
     })
  

describe.only ( 'Validar extracción de data CRM', ()=> {
    cy.once('uncaught:exception', () =>false)


    it('Extraer data CRM', ()=>{
        cy.visit('https://crmcloud.acity.com.pe/')
        //cy.wait(50)
        cy.get('#username').type(credentials.user)
        cy.get('#password').type(credentials.pass)
        cy.get('.form-actions > .btn').click()
        cy.get(':nth-child(1) > .dropdown-toggle > .fa').click()
        cy.get('#txtTarjeta').type(invitado.cuenta)
        cy.get('#btnBuscarLista').click()
        cy.get('.red').click()
        cy.wait(7000)



        //Sorteos
        for( let x = 3; x < 9; x+=2)
            {
                cy.wait(4000)   
                cy.get(`#tbl_promos > :nth-child(${x}) > :nth-child(1) > :nth-child(1)`).then(($promo)=> {
                    const promo = $promo.text() 
                cy.get(`#tbl_promos > :nth-child(${x}) > :nth-child(2)`).then(($puntaje)=> {
                    var premio =""
                    var puntaje =""
                    puntaje = $puntaje.text()
                cy.log(promo, puntaje)
        
                // const puntajea  = puntaje.split(/\r?\n/)[1]
                //var puntajeT  = puntaje.replace(/(\r\n|\n|\r)/gm, "").toString()
                
                premio  = puntaje.split(/(\d+(,\d+)?)/)[1].toString()

                premio = premio.replace(",","")
                //valoresCrm.push({promo, premio})
                const opcionesDisponibles  = puntaje.split(/(\d+(?!\.))/)[3]
                //valoresCrm.push({promo, opcionesDisponibles})
                const cupones  = puntaje.split(/(\d+(?!\.))/)[5]
                //valoresCrm.push({promo, cupones})
                const puntos  = puntaje.split(/(\d+(?!\.))/)[7]
                //valoresCrm.push({promo, puntos})

                let sorteo = {promo, premio, opcionesDisponibles, cupones, puntos}
                valoresCrm.push(sorteo)    

        
                cy.log("DATA", premio, opcionesDisponibles, cupones, puntos)
                cy.log('ARREGLO:' + valoresCrm.length)

                // cy.fixture('users').then((users) => {
                //     expect(users.puntajeEstelar).to.equal(0)
                //     expect(users.puestoTop).to.equal(1)
                //   })
                       
                    } 
                    )
                })
            }

        //Torneos
        for( let t = 9; t < 14; t+=2)
        {
            var promo = ""
            
            cy.get(`#tbl_promos > :nth-child(${t}) > :nth-child(1) > :nth-child(1)`).then(($promo)=> 
            {
                promo = $promo.text()   })

            cy.get(`#tbl_promos > :nth-child(${t}) > :nth-child(2)`).should((_)=>{}).then(($puntajeT)=> {
                const puntajea = $puntajeT.text()
            
                    var premioTA = ""

                    cy.log(promo, puntajeT)
            
                    // const puntajea  = puntaje.split(/\r?\n/)[1]
                    var puntajeT  = puntajea.replace(/(\r\n|\n|\r)/gm, "")
                    let puntajeS  = puntajeT.split(/(\d+(,\d+)?)/)

                    cy.log("PUNTAJE S:", puntajeS)
                    //valoresCrm.promo = promo
                    
                    var premioT = puntajeT.split(/(\d+(,\d+)?)/)[1]
                    cy.log("PUNTAJE T", puntajeT)
                    if(!premioT )
                    {
                        var estado = "Inactivo"
                        cy.log(promo, estado)
                        
                        let torneoA = {promo, estado}
                        valoresCrm.push(torneoA)
                        cy.log(valoresCrm)

                    }
                    else{var premioTA = premioT.replace(",","")
                
                         
                    cy.log("TORNEOS: ", premioT)
                    cy.log("TORNEOS: ", premioTA)
                
                    // //valoresCrm.promo.premio =premio
                    // const puesto  = puntajeT.split(/(\d+(?!\.))/)[5]
                    // cy.log("PUESTO: ", puesto)

                    let puestoN
                    let puesto
                    if(!puntajeS){
                        cy.log("NO EXISTE ARREGLO PUESTOS")
                    }else{
                        cy.log("PUNTAJE S :",puntajeS)
                        cy.log(puntajeS.length)
                        puntajeS.forEach(element => {
                            cy.log({element})})
                            
                        puntajeS.forEach(element => {
                            if (!element) {return}
                            else{
                                if (element.match(/.*Posición.*/)){
                                    puestoN = element.match(/.*Posición.*/)
                                    cy.log("ELEMENTO TORNEO posicion: ",puestoN)
                                    let puestoV = puntajeS.indexOf(puestoN[0])
                                
                                    cy.log("POSICION: ",puestoV)
                                
                                    puesto= puntajeS[puestoV+1]
                                    cy.log("PUESTO",puesto)
                                }

                            }

                           
                        })
                    }


                    
                    let puntosN
                    let puntos
                    if(!puntajeS){
                        cy.log("NO EXISTE ARREGLO PUESTOS")
                    }else{
                        cy.log("PUNTAJE S :",puntajeS)
                        cy.log(puntajeS.length)
                        puntajeS.forEach(element => {
                            cy.log({element})})
                            
                        puntajeS.forEach(element => {
                            if (!element) {return}
                            else{
                                if (element.match(/.*Puntos.*/)){
                                    puntosN = element.match(/.*Puntos.*/)
                                    cy.log("ELEMENTO TORNEO puntos: ",puntosN)
                                    let puntosV = puntajeS.indexOf(puntosN[0])
                                
                                    cy.log("POSICION: ",puntosV)
                                
                                    puntos= puntajeS[puntosV+1]
                                    cy.log("puntos",puntos)
                                }

                            }

                           
                        })
                    }
                
                    
                    // //valoresCrm.promo.puesto =puesto
                    // puntos  = puntajeT.split(/(\d+(?!\.))/)[7]
                    // cy.log("PUNTAJE: ", puntos)
                    // if( !puntos){
                    //     puntos = "0"
                    // }
                    //valoresCrm.promo.puntos = puntos
                    let torneo = {promo, premioTA, puesto, puntos}
                    valoresCrm.push(torneo)
        
        
            
                    cy.log("DATA", premioT, puntos)
}
                    
                    
                                    
                
          
          
            

           
                // cy.fixture('users').then((users) => {
                //     expect(users.puntajeEstelar).to.equal(0)
                //     expect(users.puestoTop).to.equal(1)
                //   })
            
                   
                } 
                )
                         
        cy.log('ARREGLO:' + valoresCrm.length)
        cy.log(valoresCrm)
        cy.writeFile('cypress/fixtures/dataCRM.json', valoresCrm)
       
            
        }
   
        

        // var cadena = valoresCrm
        //     // esta es la palabra a buscar
        // var termino = "estelar"
    
        // var posicion = valoresCrm[3].promo.indexOf(termino)
        // if (posicion !== -1)
        //     cy.log("La palabra está en la posición " + posicion)
        // else
        //     cy.log("No encontré lo que estás buscando")
        
        


            
    }) 


    it("Validar data Sorteo Estelar en CRM", ()=> {
        cy.readFile('cypress/fixtures/dataWeb.json').its('puntajeEstelar').should('eq', valoresCrm[0].puntos )
            cy.readFile('cypress/fixtures/dataWeb.json').its('cuponEstelar').should('eq', valoresCrm[0].cupones )
            cy.readFile('cypress/fixtures/dataWeb.json').its('opcionesDisponiblesEstelar').should('eq', valoresCrm[0].opcionesDisponibles )

    })

    it("Validar data Sorteo Vip en CRM", ()=> {
        
        cy.readFile('cypress/fixtures/dataWeb.json').its('puntosVip').should('eq', valoresCrm[1].puntos)
        cy.readFile('cypress/fixtures/dataWeb.json').its('cuponVip').should('eq', valoresCrm[1].cupones)
        cy.readFile('cypress/fixtures/dataWeb.json').its('opcionesDisponiblesVip').should('eq', valoresCrm[1].opcionesDisponibles )

      
    })

    it("Validar data Sorteo Sueños en CRM", ()=> {

        cy.readFile('cypress/fixtures/dataWeb.json').its('puntosSueños').should('eq', valoresCrm[2].puntos )
        cy.readFile('cypress/fixtures/dataWeb.json').its('cuponSueños').should('eq', valoresCrm[2].cupones )
        cy.readFile('cypress/fixtures/dataWeb.json').its('opcionesDisponiblesSueños').should('eq', valoresCrm[2].opcionesDisponibles )
      
    })

    it("Validar data Top Atlantic en CRM", ()=> {
        cy.readFile('cypress/fixtures/dataWeb.json').its('puestoTop').should('eq', valoresCrm[3].puesto )
        cy.readFile('cypress/fixtures/dataWeb.json').its('puntajeTop').should('eq', valoresCrm[3].puntos )
        cy.readFile('cypress/fixtures/dataWeb.json').its('premioTop').should('eq', valoresCrm[3].premioTA )
    })

    it("Validar data MegaTorneo en CRM", ()=> {
    
        if( "megaTorneo" in cy.readFile('cypress/fixtures/dataWeb.json')){
            cy.readFile('cypress/fixtures/dataWeb.json').its('torneoMega').should('eq', 'TORNEO INACTIVO'  )
            cy.log("Torneo Inactivo")
        }
        else{
            cy.readFile('cypress/fixtures/dataWeb.json').its('puestoMega').should('eq', valoresCrm[4].puesto )
            cy.readFile('cypress/fixtures/dataWeb.json').its('puntosMega').should('eq', valoresCrm[4].puntos )
            cy.readFile('cypress/fixtures/dataWeb.json').its('premioMega').should('eq', valoresCrm[4].premioTA )

        }
       
     

      
    })

    it("Validar data Torneo Mesas en CRM", ()=> {

        const prueba = cy.readFile('cypress/fixtures/dataWeb.json')


        cy.readFile('cypress/fixtures/dataWeb.json').its('torneoMesas').should((_) => {}).then(($btn) => {
            if ($btn.length) {

                cy.readFile('cypress/fixtures/dataWeb.json').its('torneoMesas').should('eq', 'TORNEO INACTIVO'  )
                cy.log("Torneo Inactivo")

    
            } else{

                cy.readFile('cypress/fixtures/dataWeb.json').its('puestoMesas').should('eq', valoresCrm[4].puesto )
                cy.readFile('cypress/fixtures/dataWeb.json').its('puntosMesas').should('eq', valoresCrm[4].puntos )
                cy.readFile('cypress/fixtures/dataWeb.json').its('premioMesas').should('eq', valoresCrm[4].premioTA )
                }
            
            
            
        } ) 
    
     
    })
      
        
     
    })


  


    

// 


describe('Validar extracción de data Websol', ()=> {
        it( 'Login y extracción de data', ()=>{
            cy.visit('https://websol.acity.com.pe/isol/')
            cy.wait(500)
            cy.get('#username').type(credentials.user)
            cy.get('#password').type(credentials.pass)
            
            cy.get('.btn').click()
            cy.wait(6000)
            cy.contains('Buscar Cliente').click()
            
            cy.wait(12000)
            cy.get('#PlayerCard').type(invitado.cuenta)
            cy.wait(1000)
            cy.get('#PlayerCard').type('{enter}')
            cy.wait(30000)
            let opcdispest
            let opcdispsue
            let premiotop
            let puestotop
            let opcdispovip
            let premiomega
            let puestomega
            for(let i=39; i<42; i+=2){
                cy.get(`:nth-child(${i}) > div`).then(($datawb)=> {
                    const datawb = $datawb.text() 
                    const puntajeT  = datawb.replace(/(\r\n|\n|\r)/gm, "")
        
    
                    cy.log("TODOS LOS PUNTAJES: ", puntajeT)
                    let arregloData = datawb.split(/(\d+(?!\.))/)
                    cy.log("SEPARADO: ", arregloData)
                    
                  
                    let textoEstelar
                    arregloData.forEach(elemento => {
                        if (elemento.match(/.*SORTEO ESTELAR.*/)){
                            textoEstelar = elemento.match(/.*SORTEO ESTELAR.*/)
                            cy.log("INDEX ESTELAR: ",textoEstelar)
                            let dataEstelar = arregloData.indexOf(textoEstelar[0])
                        
                            cy.log("DATA ESTELAR: ",dataEstelar)
                        
                            opcdispest= arregloData[dataEstelar+1]
                            cy.log("opcdispest",opcdispest)
                        }
                      
                    })
                   
    
                    let textoSuenos
                    arregloData.forEach(elemento => {
                        if (elemento.match(/.*SORTEO SUEÑOS.*/)){
                            textoSuenos = elemento.match(/.*SORTEO SUEÑOS.*/)
                            cy.log("INDEX SUEÑOS: ",textoSuenos)
                            let dataSuenos = arregloData.indexOf(textoSuenos[0])
                        
                            cy.log("DATA SUEÑOS: ",dataSuenos)

                            opcdispsue= arregloData[dataSuenos+1]
                            cy.log("opcdispsue",opcdispsue)

                        }
                    })
                
    
                   
                    let textoTop 
                    arregloData.forEach(elemento => {
                        if (elemento.match(/.*TOP ATLANTIC.*/)){
                            textoTop = elemento.match(/.*TOP ATLANTIC.*/)
                            cy.log("INDEX TOPB: ",textoTop)

                            let dataTop = arregloData.indexOf(textoTop[0])
                        
                            cy.log("INDEX TOPA: ",dataTop)

                            premiotop= arregloData[dataTop+1]
                            cy.log("premiotop",premiotop)
                            puestotop= arregloData[dataTop+3]
                            cy.log("puestotop",puestotop)

                        }
                    })
    
    
                    let textoVip
                    arregloData.forEach(elemento => {
                        if (elemento.match(/.*SORTEO VIP.*/)){
                            textoVip = elemento.match(/.*SORTEO VIP.*/)
                            cy.log("INDEX VIP: ",textoVip)

                            let dataVip = arregloData.indexOf(textoVip[0])
                        
                            cy.log("DATA VIP: ",dataVip)
                            opcdispovip= arregloData[dataVip+1]
                            cy.log("opcdispovip",opcdispovip)

                        }
                        
                    })
                    
    
                    let textoMega 
                    arregloData.forEach(elemento => {
                        if (elemento.match(/.*MEGA TORNEO.*/)){
                            textoMega = elemento.match(/.*MEGA TORNEO.*/)
                            cy.log("INDEX MEGA: ",textoMega)

                            let dataMega = arregloData.indexOf(textoMega[0])
                        
                            cy.log("DATA MEGA: ",dataMega)

                            premiomega= arregloData[dataMega+1]
                            cy.log("premiomega",premiomega)
                            puestomega= arregloData[dataMega+3]
                            cy.log("puestomega",puestomega)
                        }
        
                    })

                    var datawebsol1 = {opcdispest, opcdispsue,premiotop, puestotop, opcdispovip, premiomega,puestomega}

                    valoresWebsol.push(datawebsol1)
        
                    cy.writeFile('cypress/fixtures/dataWebsol.json', valoresWebsol)
                } 
        
                )}

        
          

        // cy.get(':nth-child(40) > div').then(($datawb)=> {

        //     const datawb2 = $datawb.text() 
        //     const puntajeT2  = datawb2.replace(/(\r\n|\n|\r)/gm, "")
            
        //     cy.log("TODOS LOS PUNTAJES: ", puntajeT2)
        //     let arregloData = datawb2.split(/(\d+(?!\.))/)

        //     cy.log("SEPARADO AB : ", arregloData)


        //     var vip = ab.indexOf("SORTEO VIP: ")
        //     if(vip>0){
        //         cy.log("INDEX VIP ",vip)
        //         opcdispovip= arregloData[vip+1]
        //     }else{
        //         opcdispovip="0"
        //     }

        //     var meg = arregloData.indexOf(" opc. MEGA TORNEO: S/. ")
        //     if(meg>0)
        //     {
        //          premiomega= arregloData[meg+1]
        //         cy.log("premiomega",premiomega)
        //          puestomega= arregloData[meg+3]
        //         cy.log("puestomega",puestomega)
        //     }else{
        //         premiomega="0"
        //         puestomega="0"
        //     }
           

    

        //     })
   
        })

        it("Validar data MegaTorneo en Websol", ()=> {

            if(valoresWebsol[0].puestomega){

                cy.readFile('cypress/fixtures/dataWeb.json').its('puestoMega').should('eq', valoresWebsol[0].puestomega )
                cy.readFile('cypress/fixtures/dataWeb.json').its('premioMega').should('eq', valoresWebsol[0].premiomega )

            }else {

                cy.readFile('cypress/fixtures/dataWeb.json').its('puestoMega').should('eq', valoresWebsol[1].puestomega )
                cy.readFile('cypress/fixtures/dataWeb.json').its('premioMega').should('eq', valoresWebsol[1].premiomega )

            }
     
    
        })
        it("Validar data Top en Websol", ()=> {

            if(valoresWebsol[0].puestotop){
    
            cy.readFile('cypress/fixtures/dataWeb.json').its('puestoTop').should('eq', valoresWebsol[0].puestotop )
            cy.readFile('cypress/fixtures/dataWeb.json').its('premioTop').should('eq', valoresWebsol[0].premiotop )

            }
            else{

                cy.readFile('cypress/fixtures/dataWeb.json').its('puestoTop').should('eq', valoresWebsol[1].puestotop )
                cy.readFile('cypress/fixtures/dataWeb.json').its('premioTop').should('eq', valoresWebsol[1].premiotop )

            }
        })
        it("Validar data Estelar en Websol", ()=> {

            if(valoresWebsol[0].opcdispest){
                cy.readFile('cypress/fixtures/dataWeb.json').its('opcionesDisponiblesEstelar').should('eq', valoresWebsol[0].opcdispest )

            }else {
                cy.readFile('cypress/fixtures/dataWeb.json').its('opcionesDisponiblesEstelar').should('eq', valoresWebsol[1].opcdispest )

            }

        })
        it("Validar data Sueños en Websol", ()=> {

            if(valoresWebsol[0].opcdispsue){

                cy.readFile('cypress/fixtures/dataWeb.json').its('opcionesDisponiblesSueños').should('eq', valoresWebsol[0].opcdispsue )


            }else {

                cy.readFile('cypress/fixtures/dataWeb.json').its('opcionesDisponiblesSueños').should('eq', valoresWebsol[1].opcdispsue )

            }
    
  
        
        })
        it("Validar data Vip en Websol", ()=> {

            if(valoresWebsol[1].opcdispovip){

                cy.readFile('cypress/fixtures/dataWeb.json').its('opcionesDisponiblesVip').should('eq', valoresWebsol[0].opcdispovip )

            }else {

                cy.readFile('cypress/fixtures/dataWeb.json').its('opcionesDisponiblesVip').should('eq', valoresWebsol[1].opcdispovip )
            }
            

        
        })
            
    
    }
    )




// describe('Comparacion', ()=> {
//     cy.then(function () {
//         expect(this.scoreA, 'compare scores').to.be.below(this.scoreB)
//       })


  


