 
import { credentials, invitado, valores} from '../../support/e2e.js'
import 'cypress-wait-until';

beforeEach(() => {
    cy.restoreLocalStorage();
  });
  
  afterEach(() => {
    cy.saveLocalStorage();
  });

describe( 'VALIDAR LOBBY DE PROMOCIONES', ()=> {

    it( 'Validar LOGIN COL exitoso', ()=>{       
        
        cy.visit('https://www.casinoatlanticcity.com/casino-online')
        cy.get('.headerLoginButtons_themed-login-buttons__BNdao > div > .clmc-header-button').click({ waitForAnimations: true })    
        cy.get('#user').type(credentials.usrweb[1])
        cy.get('#password').type(credentials.usrweb[1])
        cy.get('.clmc-dark-form > .clmc-btn-primary').click()
        cy.waitUntil(() => cy.get('[aria-label="Depositar"]').should('have.length', 1), { timeout: 10000 });
         cy.wait(5000)
        
    })
        
    it( 'Validar Titulo tus promociones', ()=>{       
        cy.wait(2000); 
        cy.visit('https://www.casinoatlanticcity.com/casino-online')
        cy.contains("Tus promociones")
         
    })
    it( 'Validar titulo SORTEO ESTELAR', ()=>{       
        cy.visit('https://www.casinoatlanticcity.com/casino-online')
        cy.wait(2000)
        cy.contains("Sorteo Estelar")
    })
    it( 'Validar descripción SORTEO ESTELAR', ()=>{       
        cy.visit('https://www.casinoatlanticcity.com/casino-online')
        cy.wait(2000)
        cy.contains("Sorteamos más de S/120,000 al mes")
    })
    it( 'Validar titulo MEGA TORNEO ATLANTIC', ()=>{       
        cy.visit('https://www.casinoatlanticcity.com/casino-online')
        cy.wait(2000)
        cy.contains("Mega Torneo Atlantic")
        cy.contains("Cada mes más de S/120,000 en premios")
    })
    it( 'Validar descripción de MEGA TORNEO ATLANTIC', ()=>{       
        cy.visit('https://www.casinoatlanticcity.com/casino-online')
        cy.wait(2000)
        cy.contains("Cada mes más de S/120,000 en premios")
    })
    it( 'Validar titulo de TOP ATLANTIC', ()=>{       
        cy.visit('https://www.casinoatlanticcity.com/casino-online')
        cy.wait(2000)
        cy.contains("Top Atlantic")
        cy.contains("Repartimos más de S/350,000 en premios mensuales")
    })
    it( 'Validar descripción de TOP ATLANTIC', ()=>{       
        cy.visit('https://www.casinoatlanticcity.com/casino-online')
        cy.wait(2000)
        cy.contains("Repartimos más de S/350,000 en premios mensuales")
    })
    it( 'Validar titulo de SORTEO DE TUS SUEÑOS', ()=>{       
        cy.visit('https://www.casinoatlanticcity.com/casino-online')
        cy.wait(2000)
        cy.contains("Sorteo de tus Sueños")
        cy.contains("Sorteamos S/100,000 en premios en noviembre")
    })
    it( 'Validar descripción de SORTEO DE TUS SUEÑOS', ()=>{       
        cy.visit('https://www.casinoatlanticcity.com/casino-online')
        cy.wait(2000)
        cy.contains("Sorteamos S/100,000 en premios en noviembre")
    })
    it( 'Validar titulo ATLANTIC VIP ROYAL', ()=>{       
        cy.visit('https://www.casinoatlanticcity.com/casino-online')
        cy.wait(2000)
        cy.contains("Atlantic VIP Royal")
        cy.contains("Repartimos más de S/120,000 en noviembre")
    })
    it( 'Validar descripción ATLANTIC VIP ROYAL', ()=>{       
        cy.visit('https://www.casinoatlanticcity.com/casino-online')
        cy.wait(2000)
        cy.contains("Repartimos más de S/120,000 en noviembre")
    })
    it( 'Validar titulo de TORNEO DE MESAS', ()=>{       
        cy.visit('https://www.casinoatlanticcity.com/casino-online')
        cy.wait(2000)
        cy.contains("Torneo de mesas")
    })
    it( 'Validar descripción de TORNEO DE MESAS', ()=>{       
        cy.visit('https://www.casinoatlanticcity.com/casino-online')
        cy.wait(2000)
        cy.contains("S/ 70,000 en premios mensuales")
    })
    it( 'Validar que al dar click en el hobber se mueva el carrusel', ()=>{       
        cy.visit('https://www.casinoatlanticcity.com/casino-online')
        cy.wait(2000)
        cy.get('.styles_contenedorLobby__PT5LI > .sliderLobby > .sliderLobby-header > :nth-child(1) > .sliderLobby-buttons > :nth-child(2)').click();
    })
    
    
    it( 'Validar titulo de DROPS AND WINS', ()=>{       
        cy.visit('https://www.casinoatlanticcity.com/casino-online')
        cy.wait(2000)
        cy.contains("DROPS AND WINS")
    })
    it( 'Validar descripción de DROPS AND WINS', ()=>{       
        cy.visit('https://www.casinoatlanticcity.com/casino-online')
        cy.wait(2000)
        cy.contains("S/2,000,000 en premios")
    })
    it( 'Validar titulo de TORNEO DE CUOTAS', ()=>{       
        cy.visit('https://www.casinoatlanticcity.com/casino-online')
        cy.wait(2000)
        cy.contains("Torneo de Cuotas")
    })
    it( 'Validar descripción de TORNEO DE CUOTAS', ()=>{       
        cy.visit('https://www.casinoatlanticcity.com/casino-online')
        cy.wait(2000)
        cy.contains("Más de S/50,000 en premios semanales")
    })
    it( 'Validar titulo de TORNEO WINNER DE WINNERS', ()=>{       
        cy.visit('https://www.casinoatlanticcity.com/casino-online')
        cy.wait(2000)
        cy.contains("TORNEO WINNER DE WINNERS")
    })
    it( 'Validar descripción de TORNEO WINNER DE WINNERS', ()=>{       
        cy.visit('https://www.casinoatlanticcity.com/casino-online')
        cy.wait(2000)
        cy.contains("Cada semana más de S/10,000 en premios")
    })
    it( 'Validar titulo de PAGO ANTICIPADO', ()=>{       
        cy.visit('https://www.casinoatlanticcity.com/casino-online')
        cy.wait(2000)
        cy.contains("Pago Anticipado")
        cy.contains("2 goles de diferencia y ya ganaste")
    })
    it( 'Validar descripción de PAGO ANTICIPADO', ()=>{       
        cy.visit('https://www.casinoatlanticcity.com/casino-online')
        cy.wait(2000)
        cy.contains("2 goles de diferencia y ya ganaste")
    })
})