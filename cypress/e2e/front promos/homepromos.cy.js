import { credentials, invitado, valores} from '../../support/e2e.js'
import 'cypress-wait-until';

beforeEach(() => {
    cy.restoreLocalStorage();
  });
  
  afterEach(() => {
    cy.saveLocalStorage();
  });

describe( 'VALIDAR HOME DE PROMOCIONES "MIS PROMOCIONES"', ()=> {

    it( 'Validar LOGIN COL exitoso', ()=>{       
        
        cy.visit('https://www.casinoatlanticcity.com/casino-online')
        cy.get('.headerLoginButtons_themed-login-buttons__BNdao > div > .clmc-header-button').click({ waitForAnimations: true })    
        cy.get('#user').type(credentials.usrweb[2])
        cy.get('#password').type(credentials.usrweb[2])
        cy.get('.clmc-dark-form > .clmc-btn-primary').click()
        cy.waitUntil(() => cy.get('[aria-label="Depositar"]').should('have.length', 1), { timeout: 10000 });
        cy.visit('https://www.casinoatlanticcity.com/casino-online/promociones')
        cy.wait(6000)
         //cambio ID
         cy.get('.styles_openButton__oFAO0').click({force: true})
         cy.get('.styles_boxChangeId__NAAMr > input').clear().type(invitado.idOrigen)
         cy.get('.styles_boxChangeId__NAAMr > button').click({force: true})

         cy.wait(3500)
        
    })
    it( 'Validar titulo Mis promociones activas', ()=>{       
        cy.wait(2000); 
        cy.visit('https://www.casinoatlanticcity.com/casino-online/promociones')
        cy.contains("Mis promociones activas")
         
    })
    it( 'Validar titulo Top Atlantic', ()=>{       
        cy.wait(3500)
        cy.visit('https://www.casinoatlanticcity.com/casino-online/promociones')
        cy.contains("Top Atlantic")
         
    })
    // it( 'Validar descripción Top Atlantic', ()=>{       
    //     cy.wait(3500)
    //     cy.visit('https://www.casinoatlanticcity.com/casino-online/promociones')
    //     cy.get('.torneo_header__X7vvD > p').should('have.text', 'S/ 9,200 en premios semanales');     
         
    // })
    it( 'Validar titulo Sorteo Sueños', ()=>{       
        
        cy.visit('https://www.casinoatlanticcity.com/casino-online/promociones')
        cy.contains("Sorteo de tus Sueños")
         
    })
    // it( 'Validar descripción Sorteo Sueños', ()=>{       
        
    //     cy.visit('https://www.casinoatlanticcity.com/casino-online/promociones')
    //     cy.get(':nth-child(4) > .flip_frontContainer__99Imr > .styles_content__PLJ6H > .sorteo_contentContainer__yhfVg > .sorteo_activoContainer__kLMUu > .sorteo_activoHeader__YU5iW > span').should('have.text', 'S/ 25,000 en premios todas las semanas');     
         
    // })
    it( 'Validar titulo Sorteo Estelar', ()=>{       
        
        cy.visit('https://www.casinoatlanticcity.com/casino-online/promociones')
        cy.contains("Sorteo Estelar")
         
    })
    // it( 'Validar descripción Sorteo Estelar', ()=>{       
        
    //     cy.visit('https://www.casinoatlanticcity.com/casino-online/promociones')
    //     cy.get(':nth-child(3) > .flip_frontContainer__99Imr > .styles_content__PLJ6H > .sorteo_contentContainer__yhfVg > .sorteo_activoContainer__kLMUu > .sorteo_activoHeader__YU5iW > span').should('have.text', 'S/ 7,500 repartidos en cada sorteo');     
         
    // })
    it( 'Validar titulo Sorteo Vip Royal', ()=>{       
        
        cy.visit('https://www.casinoatlanticcity.com/casino-online/promociones')
        cy.contains("Atlantic Vip Royal")
         
    })
    // it( 'Validar descripción Vip Royal', ()=>{       
        
    //     cy.visit('https://www.casinoatlanticcity.com/casino-online/promociones')
    //     cy.get('.torneo_header__X7vvD > p').should('have.text', 'S/ 9,200 en premios semanales');     
         
    // })
    it( 'Validar titulo Atlantic Delivery', ()=>{       
        
        cy.visit('https://www.casinoatlanticcity.com/casino-online/promociones')
        cy.contains("Delivery de la Semana")
         
    })
    // it( 'Validar descripción Atlantic Delivery', ()=>{       
        
    //     cy.visit('https://www.casinoatlanticcity.com/casino-online/promociones')
    //     cy.get('.delivery_header__Jq2k2 > span').should('have.text', 'Acumula 800 puntos y disfruta de un delicioso almuerzo.');     
         
    // })
   
})